Development Setup
=================

Here are the steps you need to take to get up and running with development tasks.

Backend
-------

The backend is written in python and uses ``fastapi`` as light weight framework.
To get started you will need a current version of python (>= 3.8) and `poetry <https://python-poetry.org/docs/#installation>`_.

Change into the project folder (the one with the ``pyproject.toml`` file in it) and run
``poetry install``. This will install all backend dependencies and get you ready to run.

`nox <https://nox.thea.codes/en/stable/>`_ is used for automation of common tasks and you can find detail

We also strongly encourage you to use `pre-commit <https://pre-commit.com/>`_ which will catch most problems before you commit.
This decreases cycle times. All you need to do is ``poetry run pre-commit install``.

Prerequisites
^^^^^^^^^^^^^

At the moment you still need 1 external thing:

- a factcast database to talk to

This is at the moment not provided as mock but we will do so down the line.

Running the Backend
^^^^^^^^^^^^^^^^^^^

Configure the application in accordance to the :ref:`quickstart:Quickstart Guide`.
Run ``poetry run uvicorn fact_explorer.app.main:app --reload --port 8080 --log-level debug`` and the backend will start.

Running Tests
^^^^^^^^^^^^^

Just type ``poetry run pytest``


Installing Optional Dependencies
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

If you want to be able to develop against the full feature-set you will need to install
the additional optional dependencies ``cryptoshred`` and ``schema-registry`` and perform
the configuration as per the quickstart guide. This means you will also need the infrastructure
that goes along with this. For details on what that is, please look at the documentation of the
corresponding projects.


Frontend
--------

The frontend is a `next app <https://nextjs.org/>`_. To get up and running you simply change into the
frontend folder ``src/fact_explorer/frontend`` and run

- ``npm install``
- ``npm run dev``

and open ``http://localhost:3000`` in your local browser.
There is a more detailed Readme on the frontend setup in that folder as well.
More details can be found in the ``README`` in the frontend folder.

Documentation
-------------

You can run ``poetry run nox -s docs -- serve`` in your shell. This will automatically open
a reloading version of the docs in a browser window. This makes it easier to change documentation.
