This is the frontend for the Fact Explorer.

# Prequisites

Please have a recent version of [Node](https://nodejs.org) installed.

# Development Instructions

Run `$ npm install` for the initial setup (or whenever dependencies change).

Run `$ npm run dev` to start the local development server which will be available on [localhost](http://localhost:3000/).

You can run `$ npm run lint` for some basic linter checks and `$ npm run format` to automatically apply formatting.
