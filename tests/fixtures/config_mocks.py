import pytest

from os import environ as env


@pytest.fixture(scope="function")
def app_config():
    env["DATABASE_PASSWORD"] = "nope"
    env["CRYPTOSHRED_INIT_VECTOR_PATH"] = "nope"

    yield

    del env["DATABASE_PASSWORD"]
    del env["CRYPTOSHRED_INIT_VECTOR_PATH"]
