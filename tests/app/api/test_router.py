from fastapi.testclient import TestClient
import pytest


@pytest.fixture(scope="function")
def client(fe_dir, mock_decryptor, app_config):
    from fact_explorer.app.main import app

    return TestClient(app)


def test_features(client):
    res = client.get("/api/features")
    assert res.status_code == 200
    assert res.json() == {"cryptoshred": True, "schema_registry": True}
