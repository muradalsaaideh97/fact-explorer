import pytest
import sys
import logging
from os import environ as env


@pytest.mark.asyncio
@pytest.mark.parametrize(
    "function_name,function_params,expected",
    [
        ("schema", {"namespace": "test", "type": "nope", "version": 1}, {}),
        ("namespaces", {}, []),
    ],
)
async def test_does_nothing_without_schema_reg(
    mocker, caplog, app_config, function_name, function_params, expected
):
    if "schema_registry.registries.HttpRegistry" in sys.modules:
        del sys.modules["schema_registry.registries.HttpRegistry"]
        del sys.modules["schema_registry.registries.entities"]
    if "fact_explorer.app.business.registry" in sys.modules:
        del sys.modules["fact_explorer.app.business.registry"]

    mocker.patch.dict(
        "sys.modules",
        {
            "schema_registry.registries": None,
        },
    )
    import fact_explorer.app.business.registry as reg

    with caplog.at_level(logging.DEBUG):
        func = getattr(reg, function_name)
        res = await func(**function_params)

    assert res == expected
    assert "Schema Registry not installed" in caplog.text


@pytest.mark.asyncio
async def test_emits_warning_if_schema_reg_is_configured_but_not_available(
    mocker, caplog, app_config
):
    if "schema_registry.registries.HttpRegistry" in sys.modules:
        del sys.modules["schema_registry.registries.HttpRegistry"]
        del sys.modules["schema_registry.registries.entities"]
    if "fact_explorer.app.business.registry" in sys.modules:
        del sys.modules["fact_explorer.app.business.registry"]

    mocker.patch.dict(
        "sys.modules",
        {
            "schema_registry.registries": None,
        },
    )
    env["SCHEMA_REGISTRY_URL"] = "nope"

    with caplog.at_level(logging.WARNING):
        import fact_explorer.app.business.registry  # noqa: F401

    del env["SCHEMA_REGISTRY_URL"]

    assert "Schema Registry URL configured" in caplog.text


@pytest.mark.asyncio
async def test_enabled_retruns_false_without_schema_reg(mocker, app_config):
    if "schema_registry.registries.HttpRegistry" in sys.modules:
        del sys.modules["schema_registry.registries.HttpRegistry"]
        del sys.modules["schema_registry.registries.entities"]
    if "fact_explorer.app.business.registry" in sys.modules:
        del sys.modules["fact_explorer.app.business.registry"]

    mocker.patch.dict(
        "sys.modules",
        {
            "schema_registry.registries": None,
        },
    )

    import fact_explorer.app.business.registry as reg

    res = await reg.schema_registry_enabled()

    assert not res


@pytest.mark.asyncio
async def test_enabled_retruns_true_with_schema_reg(app_config):
    import fact_explorer.app.business.registry as reg

    res = await reg.schema_registry_enabled()

    assert res


@pytest.mark.asyncio
@pytest.mark.parametrize(
    "function_name,function_params,expected",
    [
        ("schema", {"namespace": "test", "type": "nope", "version": 1}, {}),
        ("namespaces", {}, []),
        ("namespaces", {"with_types": True}, {}),
    ],
)
async def test_returns_values_with_schema_reg(
    mock_registry, app_config, function_name, function_params, expected
):
    import fact_explorer.app.business.registry as reg

    reg.registry = mock_registry

    mock_registry.get.return_value = {}
    mock_registry.example.return_value = {}

    func = getattr(reg, function_name)
    res = await func(**function_params)

    assert res == expected
